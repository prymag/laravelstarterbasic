<?php

use Illuminate\Database\Seeder;

class CategoriesSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $table = 'categories';
        DB::table($table)->delete();

        $data = array(
        [
            'name' => 'Uncategorized',
            'url_slug' => 'uncategorized',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]
        );

        DB::table($table)->insert($data);
    }

}
