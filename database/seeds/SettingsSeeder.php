<?php

use Illuminate\Database\Seeder;

class SettingsSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $table = 'settings';
        DB::table($table)->delete();

        $data = array(
            [
                'key' => 'homepage',
                'value' => '1',
                'description' => 'Homepage for the website',
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ],
            [
                'key' => 'postspage',
                'value' => '2',
                'description' => 'Posts page for the website, this will show all posts',
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ],
            [
                'key' => 'sitename',
                'value' => 'Laravel5 Starter',
                'description' => 'The name for your website',
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ]
        );

        DB::table($table)->insert($data);
    }

}
