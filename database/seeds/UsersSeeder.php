<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
        $table = 'users';
        DB::table($table)->delete();

        $data = array([
            'username' => 'admin',
            'password' => Hash::make('admin1234'),
            'email' => 'admin@mail.com',
            'firstname' => 'John',
            'lastname' => 'Doe',
            'usertype' => '1',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);

        DB::table($table)->insert($data);
	}

}
