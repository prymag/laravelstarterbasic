<?php

use Illuminate\Database\Seeder;

class PostsSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $table = 'posts';
        DB::table($table)->delete();

        $data = array(
        [
            'title' => 'Homepage',
            'content' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus porta sodales augue at posuere. Ut tristique augue sit amet feugiat tincidunt. Aliquam erat volutpat. Mauris a dui interdum, vestibulum augue nec, vulputate velit. Fusce gravida magna varius felis tincidunt mollis. Vivamus sed nisi et odio sollicitudin accumsan. Donec quis nulla quis erat malesuada posuere at nec ante. Nunc sodales metus eu tellus tristique, id dignissim libero efficitur. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>

<p>Sed quam magna, posuere id scelerisque vitae, faucibus a velit. In consectetur sodales tellus, vel aliquam tortor volutpat vel. Suspendisse in interdum turpis, auctor scelerisque ex. Duis porttitor, nulla et feugiat pulvinar, felis nulla mollis odio, varius laoreet lectus magna eu ante. Praesent ut dolor id eros hendrerit varius. Pellentesque at elit elit. Mauris eget pretium orci. Donec id massa ut nibh tincidunt pulvinar a id erat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean et vestibulum enim. Etiam non erat finibus massa molestie fringilla vel in dolor. Pellentesque rhoncus massa neque, in fermentum dolor vehicula vitae. Fusce lobortis, dui quis feugiat molestie, lacus purus dignissim ante, aliquam dapibus tellus lectus id nulla.</p>

<p>Praesent ut purus in massa porttitor commodo. Phasellus sagittis volutpat nisl vel facilisis. Sed elementum pharetra massa id egestas. Nulla eu ullamcorper magna. Cras magna nibh, consequat quis gravida quis, bibendum eget diam. Maecenas aliquet, metus sed pretium porttitor, nulla enim aliquet neque, ut faucibus lorem sapien ac augue. Donec efficitur massa nibh, nec ultricies enim dapibus at. Vestibulum mollis dignissim elit a commodo.</p>

<p>Fusce lacinia justo nisl, quis efficitur erat mollis a. Integer tempor, leo nec maximus ullamcorper, augue mauris porta lacus, nec vehicula tortor ex et augue. Maecenas congue id enim non ornare. Nullam consequat, mi id posuere cursus, lorem elit hendrerit ligula, vitae tincidunt purus nulla vel est. Cras erat ante, eleifend in sapien commodo, malesuada varius purus. Proin purus elit, vestibulum vel malesuada a, laoreet sed lacus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Cras fringilla, diam vel facilisis volutpat, nibh enim egestas justo, tincidunt sodales odio est ac libero. In hac habitasse platea dictumst. Sed dapibus turpis quis mi fermentum, nec efficitur justo posuere. Aenean dui nibh, egestas et enim pellentesque, dictum interdum orci. Cras consectetur lectus vel risus porta feugiat.</p>

<p>Vivamus laoreet dolor aliquet, facilisis metus vitae, interdum turpis. Etiam rhoncus condimentum nulla, a imperdiet enim pretium non. Aenean consectetur venenatis molestie. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut eget orci quis arcu bibendum vestibulum in et arcu. Vestibulum sit amet quam fringilla, convallis diam a, dignissim massa. Vestibulum laoreet erat et velit venenatis fringilla. Aliquam ultrices mi urna, et suscipit ante pulvinar non. Vestibulum condimentum risus tempus, faucibus odio ac, rhoncus turpis.</p>',
            'url_slug' =>  'homepage',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime(),
            'post_type' => 'page'
        ],
        [
            'title' => 'Blogs',
            'content' => '',
            'url_slug' => 'blogs',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime(),
            'post_type' => 'page'
        ],
        [
            'title' => 'What Is Lorem Ipsum?',
            'content' => '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>',
            'url_slug' =>  'what-is-lorem-ipsum',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime(),
            'post_type' => 'post'
        ],
        [
            'title' => 'Why do we use it?',
            'content' => '<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>',
            'url_slug' =>  'why-do-we-use-it',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime(),
            'post_type' => 'post'
        ],

        );

        DB::table($table)->insert($data);
    }

}
