$(document).ready(function () {

    // Check all table items
    $('#check_all_table_items').click( function() {
        $('.check-all-table-item').prop('checked', $(this).prop('checked') );
    })

    // Checks a table item and unset all checked one, then prompt for delete.
    $('.del-table-itm').click( function(e) {
        e.preventDefault();

        var itm = $(this);
        bootbox.confirm("Are you sure?", function(result) {

            if( result ){

                var target_itm = $(itm).data('target');
                var target_form = $(itm).data('target-form');

                $('.check-all-table-item').prop('checked', false);
                $(target_itm).prop('checked', true);
                $(target_form).submit();

            }

        });

    });

    // Handle submit for show_item_form
    $('.show-items').change( function() {
        $('#show_item_form').submit();
    });

    // Handle data item deletion
    $('.del-data-entry').click(function () {
        var target_form = $(this).data('target-form');

        bootbox.confirm("Are you sure?", function (r) {
            if( r ){
                $(target_form).find('#form_method').val('DELETE');
                $(target_form).submit();
            }
        });

    });




})
