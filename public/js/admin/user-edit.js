$('document').ready(function (){
    // Handle Avatar selection
    $('.avatar-list').on('click', '.avatar-item', function(e){
        e.preventDefault();

        $('.avatar-item.selected').removeClass('selected');
        $(this).addClass('selected');


        // Check if this is a custom image, if it is show delete button for the image
        if( $(this).hasClass('custom') && !$(this).hasClass('active') && !$(this).hasClass('current') ){
            $('#delete_avatar').prop('disabled', false);
        }
        else {
            $('#delete_avatar').prop('disabled', true);
        }

        // Check if the Use Image is disabled, enable it if an item is selected
        if( $('#use_image').prop('disabled') )
            $('#use_image').prop('disabled', false);

    });

    $('#delete_avatar').click(function () {
        // Disable button to prevent double clicking
        var btn = $(this);
        btn.prop('disabled', true);

        var selected_avatar = $('.avatar-item.custom.selected');
        var image_name = selected_avatar.data('image-name');
        var user_id = $('#user_id').val();

        $.ajax({
            url:'/admin/ajax/delete-user-image',
            data:'image_name='+image_name+'&user_id='+user_id,
            method:'POST',
            beforeSend: function( xhr ) {
                xhr.setRequestHeader('X-CSRF-TOKEN', getToken());
            }
        }).done( function (data) {
            if( data == 'Success'){
                selected_avatar.parent().fadeOut(500).delay().remove();
                // Hide delete button if no avatar items left
                console.log($('.avatar-list.custom li').length)
                if( $('.avatar-list.custom li').length == 0 ){
                    btn.hide();

                    // Disable Use Image button if no item has been selected
                    if( $('.avatar-list.current').length == 0 )
                        $('#use_image').prop('disabled', true);

                }
            }
        })
    });


    $('#use_image').click(function (){

        var image_name = $('.avatar-item.selected').data('image-name');
        var use_custom_image = $('.avatar-item.selected').hasClass('custom') ? 1 : 0;
        var user_id = $('#user_id').val();
        var image;

        if( use_custom_image == 1 )
            image = '/images/users/'+user_id+'/'+image_name;
        else
            image = '/images/avatars/' + image_name;

        if( image != '') {

            // Add a current class to the selected image which will be used to handle custom image deletion
            $('.avatar-item.current').removeClass('current');
            $('.avatar-item.selected').addClass('current');

            $('#selected_user_image').val(image_name);
            $('#use_custom_image').val(use_custom_image);
            $('#edit_profile_pic').find('img').attr('src', window.location.origin + image);

            // Reset Values
            $('#delete_avatar').prop('disabled', true);
            $('#fupload_progress .progress-bar').css('width', 0);
            $('#user_image_name').val('');

            $('#myModal').modal('hide');


        }

    });
});

/*jslint unparam: true */
/*global window, $ */
$(function () {
    'use strict';

    // Change this to the location of your server-side upload handler:
    var url =  'http://' + window.location.hostname + '/admin/ajax/upload-user-image';


    var uploadButton = $('<button/>')
        .addClass('btn btn-success')
        .attr('id', 'uploadBtn')
        .prop('disabled', true)
        .text('Processing...')
        .on('click', function () {
            var $this = $(this),
                data = $this.data();
            $this
                .off('click')
                .text('Abort')
                .on('click', function () {
                    $this.remove();
                    data.abort();
                });
            data.submit().always(function () {
                $this.remove();
            });
        });

    $('#fileupload').fileupload({
        url: url,
        dataType: 'json',
        autoUpload: false,
        acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
        maxFileSize: 2000000, // 5 MB
        // Enable image resizing, except for Android and Opera,
        // which actually support image resizing, but fail to
        // send Blob objects via XHR requests:
        disableImageResize: /Android(?!.*Chrome)|Opera/
            .test(window.navigator.userAgent),
        previewMaxWidth: 100,
        previewMaxHeight: 100,
        previewCrop: true,
        imageMaxWidth: 215,
        imageMaxHeight: 215,
        imageCrop: true,
        formData:{'user_id':$('#user_id').val()},
        disableExifThumbnail:true,
        beforeSend: function(xhr, data) {
            xhr.setRequestHeader('X-CSRF-TOKEN', getToken());
        }
    })
        .on('fileuploadprogressall', function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#fupload_progress .progress-bar').css('width',progress + '%');
        })
        .on('fileuploadadd', function (e, data) {
            $('#fupload_progress .progress-bar').css('width', '0%' );
            data.context = $('<div/>').appendTo('#files');
            $.each(data.files, function (index, file) {
                $('#user_image_btn_group #uploadBtn').remove();
                $('#user_image_btn_group').prepend(uploadButton.clone(true).data(data));
                $('#user_image_name').val(file.name);

            });
        })
        .on('fileuploadprocessalways', function (e, data) {

            var index = data.index,
                file = data.files[index],
                node = $(data.context.children()[index]);
            if (file.preview) {
                $('#preview').html('').prepend(file.preview).prepend('<p>Preview</p>');
                $('#preview').find('canvas').css({'border-radius':'50%'});
            }
            if (file.error) {
                node.append('<br>').append($('<span class="text-danger"/>').text(file.error));
            }
            if (index + 1 === data.files.length) {
                $('#user_image_btn_group').find('button').text('Upload').prop('disabled', !!data.files.error);

            }
        })
        .on('fileuploaddone', function (e, data) {


            var avatar_item = '<li class="col-md-2 col-sm-2" id="custom_avatar">\
                <a href="#" class="avatar-item custom" data-image-name="'+data.result[0].name+'">\
            <img src="'+data.result[0].url+'" alt="avatar" class="img-circle" />\
            <span class="fa fa-check"></span>\
            </a>\
            </li>';

            $('.avatar-list.custom').append(avatar_item);

            // Check if delete button is visible
            if( !$('#delete_avatar').is(':visible') ){
                $('#delete_avatar').show();
            }

        })
        .on('fileuploadfail', function (e, data) {
            console.log('File upload failed');
        })
        .prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
});

function getToken(){
    return $('#_token').val();
}