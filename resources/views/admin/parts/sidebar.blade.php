    <!-- Left side column. contains the logo and sidebar -->


      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">



          {{-- Main Menu --}}
          <ul class="sidebar-menu">
            <li class="header">NAVIGATION</li>

            <li class="@if( $pagename == 'dashboard' ) {{ 'active' }} @endif">
                <a href="{{ url('admin') }}">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                </a>
            </li>

            <li class="@if( $pagename == 'users') {{ 'active' }} @endif">
                <a href="{{ url('admin/users/') }}">
                    <i class="fa fa-users"></i> <span>Users</span>
                </a>
            </li>

            <li class="@if( $pagename == 'pages') {{ 'active' }} @endif">
                <a href="{{ url('admin/pages/') }}">
                    <i class="fa fa-file"></i> <span>Pages</span>
                </a>
            </li>

            <li class="@if( $pagename == 'posts' || $pagename == 'post-categories') {{ 'active' }} @endif treeview">
                <a href="#">
                    <i class="fa fa-thumb-tack"></i> <span>Posts</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li class="@if( $pagename == 'posts' ) {{ 'active' }} @endif"><a href="{{ url('admin/posts') }}"><i class="fa fa-circle-o"></i> All Posts</a></li>
                    <li class="@if( $pagename == 'post-categories') {{ 'active' }} @endif"><a href="{{ url('admin/post-categories') }}"><i class="fa fa-circle-o"></i> Categories</a></li>
                </ul>
            </li>

            <li class="@if( $pagename == 'settings') {{ 'active' }} @endif">
                <a href="{{ url('admin/settings/') }}">
                    <i class="fa fa-gear"></i> <span>Site Settings</span>
                </a>
            </li>

          </ul>
          {{-- End Main Menu --}}


        </section>
        <!-- /.sidebar -->
      </aside>