        <!-- jQuery 2.1.4 -->
        <script src="{{ asset('plugins/jQuery/jQuery-1.11.1.min.js') }}"></script>
        <!-- jQuery UI 1.11.2 -->
        <script src="{{ asset('plugins/jQueryUI/jquery-ui.min.js') }}" type="text/javascript"></script>
        <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
        <script>
          $.widget.bridge('uibutton', $.ui.button);
        </script>
        <!-- Bootstrap 3.3.2 JS -->
        <script src="{{ asset('plugins/bootstrap/bootstrap.js') }}" type="text/javascript"></script>
        <!-- Morris.js charts -->
        <script src="{{ asset('plugins/raphael/raphael-min.js') }}"></script>
        <script src="{{ asset('plugins/morris/morris.min.js') }}" type="text/javascript"></script>
        <!-- Sparkline -->
        <script src="{{ asset('plugins/sparkline/jquery.sparkline.min.js') }}" type="text/javascript"></script>
        <!-- jvectormap -->
        <script src="{{ asset('plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}" type="text/javascript"></script>
        <!-- jQuery Knob Chart -->
        <script src="{{ asset('plugins/knob/jquery.knob.js') }}" type="text/javascript"></script>
        <!-- daterangepicker -->
        <script src="{{ asset('plugins/moment/moment.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/daterangepicker/daterangepicker.js') }}" type="text/javascript"></script>
        <!-- datepicker -->
        <script src="{{ asset('plugins/datepicker/bootstrap-datepicker.js') }}" type="text/javascript"></script>
        <!-- Bootstrap WYSIHTML5 -->
        <script src="{{ asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}" type="text/javascript"></script>
        <!-- Slimscroll -->
        <script src="{{ asset('plugins/slimScroll/jquery.slimscroll.min.js') }}" type="text/javascript"></script>
        <!-- FastClick -->
        <script src="{{ asset('plugins/fastclick/fastclick.min.js') }}"></script>
        <!-- AdminLTE App -->
        <script src="{{ asset('js/admin/app.min.js') }}" type="text/javascript"></script>

        <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
        <script src="{{ asset('js/admin/dashboard.js') }}" type="text/javascript"></script>

        <!-- AdminLTE for demo purposes -->
        <script src="{{ asset('js/admin/demo.js') }}" type="text/javascript"></script>

        <script type="text/javascript" src="{{ asset('plugins/bootbox/bootbox.min.js') }}"></script>

        <!-- Admin Scripts -->
        <script src="{{ asset('js/admin/scripts.js') }}" type="text/javascript"></script>