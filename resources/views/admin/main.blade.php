<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    @include('parts/head')
    @yield('styles')
  </head>
  <body class="skin-blue sidebar-mini">
      <div class="wrapper">

        @include('parts/header')

        @include('parts/sidebar')

        @yield('content')

        @include('parts/footer')



      </div>

      @include('parts/scripts-default')
      @yield('scripts') {{-- Load additional scripts --}}

  </body>
  </html>