@if( !empty($pages) )

    <?php
        $space = '';
        if( $ctr > 0  ){
            for($i = 0; $i < $ctr; $i++)
                $space .= '&nbsp;-&nbsp;';
        }
    ?>

    <option @if($selected == $pages->id){{ 'selected="selected"' }}@endif value="{{ $pages->id }}">{{ $space . $pages->title }}</option>

    <?php $child = $pages->children()->get(); ?>
    @if( $child->count() > 0 )
        @foreach($child as $child_page)
            @include('admin/pages/settings/recursion/posts-pages', ['pages' => $child_page, 'ctr' => $ctr+=1])
        @endforeach
    @endif


@endif