@extends('admin/main')

@section('content')

    <div class="content-wrapper">

        <section class="content-header">
            <h1>Site Settings</h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('admin') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li class="active"><i class="fa fa-gear"></i> Site Settings</li>
            </ol>
        </section>


        <section class="content">

            @include('messages/message')

            {!! Form::open() !!}

            <div class="row">
            <div class="col-xs-4">

                <div class="box box-primary">
                    <div class="box-header">

                    </div>
                    <div class="box-body">

                        <div class="form-group">
                            <label for="sitename">Sitename</label>
                            <input class="form-control" type="text" name="sitename" id="sitename" value="{{ $settings->where('key', 'sitename')->first()->value }}"/>
                        </div>

                        <div class="form-group">
                            <label for="homepage">Home Page</label>
                            <select name="homepage" id="homepage" class="form-control">
                                <option></option>
                                @if( !empty($pages) )
                                    @foreach($pages as $p)
                                        @include('admin/pages/settings/recursion/posts-pages', ['pages' => $p, 'ctr' => 0, 'selected' => $settings->where('key', 'homepage')->first()->value ])
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        
                        <div class="form-group">
                            <label for="postpage">Posts Page</label>
                            <select class="form-control" name="postspage" id="postspage">
                                <option></option>
                                @if( !empty($pages) )
                                    @foreach($pages as $p)
                                        @include('admin/pages/settings/recursion/posts-pages', ['pages' => $p, 'ctr' => 0, 'selected' => $settings->where('key', 'postspage')->first()->value ])
                                    @endforeach
                                @endif
                            </select>
                        </div>                        
                        
                    </div>
                </div>


            </div>
            </div>

            <div class="row">

                <div class="col-xs-12">
                    <input type="submit" name="save_settings" value="Save" class="btn btn-primary"/>
                </div>

            </div>

            {!! Form::close() !!}

        </section>
    </div>

@endsection