@extends('admin/main')

@section('content')
    <?php
        $user_image = $user->use_custom_image ? 'images/users/'.$user->id.'/'.$user->user_image : 'images/avatars/'.$user->user_image;
    ?>
    <div class="content-wrapper">

    <section class="content-header">
        <h1>Profile <small>Edit</small></h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('admin/dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active">Edit Profile</li>
        </ol>
    </section>

    <section class="content">

        {{-- Show Success message if any --}}
        @include('messages/message')

        {!! Form::open( array('url' => 'admin/profile', 'autocomplete' => 'off', 'id' => 'user_edit_form') ) !!}

        <div class="row">
        <div class="col-xs-12 col-md-8">

            <div class="box box-primary">

                <div class="box-header">
                    <h4>Profile Info</h4>
                </div>

                <div class="box-body">

                    <div class="profile-image-edit">
                        <a href="#" id="edit_profile_pic"><img src="{{ asset($user_image) }}" class="img-circle" alt="user-img" data-toggle="modal" data-target="#myModal" /></a>
                    </div>

                    <input type="hidden" name="selected_user_image" id="selected_user_image" value="{{ $user->user_image }}" />
                    <input type="hidden" name="use_custom_image" id="use_custom_image" value="{{ $user->use_custom_image }}" />
                    <input type="hidden" name="user_id" id="user_id" value="{{ $user->id }}"/>

                    <div class="form-group">
                        <label for="username">Username</label>
                        <input class="form-control" type="text" name="username" value="{{ $user->username }}" id="username" autocomplete="off"/>
                    </div>


                    <div class="form-group">
                        <label for="password">Password</label>
                        <input class="form-control" type="password" name="password"  id="password"/>

                    </div>

                    <div class="form-group">
                        <label for="email">Email</label>
                        <input class="form-control" type="text" name="email" value="{{ $user->email }}" id="email"/>
                    </div>

                    <div class="form-group">
                        <label for="firstname">Firstname</label>
                        <input class="form-control" type="text" name="firstname" value="{{ $user->firstname }}" id="firstname"/>
                    </div>

                    <div class="form-group">
                        <label for="lastname">Lastname</label>
                        <input class="form-control" type="text" name="lastname" value="{{ $user->lastname }}" id="lastname"/>
                    </div>

                </div>
            </div> {{-- /.box.box-primary --}}

        </div>

        <div class="col-xs-12 col-md-4">
            <div class="box box-primary">
                <div class="box-header">
                    <h4>Other Info</h4>
                </div>
                <div class="box-body">
                    <p> <strong>User Type: </strong> {{ $user->user_type_string }}</p>
                    <p> <strong>Created on:</strong> {{ $user->created_at->format('M d, Y') }}</p>
                    <p> <strong>Updated on:</strong> {{ $user->updated_at->format('M d, Y') }}</p>
                </div>
            </div>
        </div>

        </div>

        <div class="row">

            <div class="col-xs-12">
            <input type="submit" name="save_profile" value="Save" class="btn btn-primary"/>
            </div>

        </div>

        {!! Form::close() !!}

    </section>
    </div> {{-- /.content-wrapper --}}

    {{-- Modal Dialog setup for Editing image --}}
    <div class="modal fade" id="myModal">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Edit Profile Image</h4>
          </div>
          <div class="modal-body">

                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Choose an Avatar</a></li>
                        <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false">Upload Custom</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            <ul class="avatar-list main row">
                                @if($avatars)
                                @foreach( $avatars as $a)
                                    <li class="col-md-2 col-sm-2">
                                        <a href="#" class="avatar-item @if($a['basename'] == $user->user_image){{ 'selected active' }}@endif" data-image-name="{{ $a['basename'] }}" >
                                        <img src="{{ url('/images/avatars/'.$a['basename']) }}" alt="avatar" class="img-circle" />
                                        <span class="fa fa-check"></span>
                                        </a>
                                    </li>
                                @endforeach
                                @endif
                            </ul>
                        </div> {{-- /#tab_1 --}}
                        <div class="tab-pane" id="tab_2">


                            <ul class="avatar-list custom row">
                                @if($user_avatars)
                                @foreach( $user_avatars as $a)
                                    <li class="col-md-2 col-sm-2">
                                        <a href="#" class="avatar-item custom @if($a['basename'] == $user->user_image){{ 'selected active' }}@endif" data-image-name="{{ $a['basename'] }}" >
                                            <img src="{{ url('/images/users/'.$user->id.'/'.$a['basename']) }}" alt="avatar" class="img-circle" />
                                            <span class="fa fa-check"></span>
                                        </a>
                                    </li>
                                @endforeach
                                @endif
                            </ul>

                            <button class="btn btn-warning btn-xs" id="delete_avatar" @if(empty($user_avatars))style="display: none"@endif disabled="disabled">Delete Selected</button>
                            <p><small>Note: for best results image must be 215x215 pixels and must not exceed 2mb</small></p>

                            <div class="input-group">

                            <input class="form-control" type="text" id="user_image_name"/>
                                <div class="input-group-btn" id="user_image_btn_group">
                                    <div class="btn btn-primary">
                                    <i class="glyphicon glyphicon-folder-open"></i> &nbsp; Browse
                                    <input id="fileupload" type="file" name="files[]">
                                    <input type="hidden" id="_token" value="{{ csrf_token() }}"/>
                                    </div>
                                </div>

                            </div>

                            <div id="fupload_progress" class="progress xxs">
                                <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar"></div>
                            </div>

                        </div> {{-- /#tab_2 --}}
                    </div> {{-- /.tab-content --}}
                </div> {{-- /.nav-tabs-custom --}}




          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary" id="use_image">Use Image</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ asset('plugins/file-upload/load-image.all.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/file-upload/canvas-to-blob.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/file-upload/jquery.iframe-transport.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/file-upload/jquery.fileupload.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/file-upload/jquery.fileupload-process.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/file-upload/jquery.fileupload-image.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/file-upload/jquery.fileupload-validate.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/admin/user-edit.js') }}"></script>
@endsection