@extends('......main')
@section('content')
     <div class="content-wrapper">

            <section class="content-header">
                <h1>Users</h1>
                <ol class="breadcrumb">
                    <li><a href="{{ url('admin') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                    <li class="active"><i class="fa fa-users"></i> Users</li>
                </ol>
            </section>




            <section class="content">
                <div class="row">
                <div class="col-xs-12">

                    {{-- Users Box Start --}}
                    <div class="box box-primary">

                        <div class="box-header">
                            <div class="row">

                            <div class="col-sm-2">
                                <a class="btn btn-block btn-primary" href="{{ url('admin/users/create') }}">Add Users</a>
                            </div>



                            </div>
                        </div>

                        <div class="box-header">
                            <div class="row">
                                <div class="col-sm-6">

                                    <div class="show-items form-inline">
                                    {!! Form::open( array('id' => 'show_item_form', 'method' => 'get') ) !!}
                                    <label>
                                    Show
                                    <select class="select-show-item form-control" name="show">
                                        <option value="25" @if( $show == '25'){{ 'selected="selected"' }}@endif>25</option>
                                        <option value="50" @if( $show == '50'){{ 'selected="selected"' }}@endif>50</option>
                                        <option value="all" @if( $show == '0'){{ 'selected="selected"' }}@endif>All</option>
                                    </select>

                                    @if( isset($page) )
                                        <input type="hidden" name="page" value="{{ $page }}"/>
                                    @elseif( isset($s) )
                                        <input type="hidden" name="search" value="{{ $s }}"/>
                                    @endif

                                    users
                                    </label>
                                    {!! Form::close() !!}
                                    </div>

                                </div>

                                <div class="col-sm-3 col-sm-offset-3">

                                    <div class="data-search">

                                    {!! Form::open(array('method' => 'get', 'url' => 'admin/users/search')) !!}
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-search"></i></span>
                                            <input class="form-control" type="text" placeholder="Search" name="search" value="{{ $s or '' }}"/>
                                            <span class="input-group-btn">
                                                <input type="submit" class="btn btn-info btn-flat" type="button" value="Go">
                                            </span>
                                        </div>
                                    {!! Form::close() !!}


                                    </div>

                                </div>

                            </div>
                        </div>

                        <div class="box-body dataTables_wrapper">

                            {{-- Show Success message if any --}}
                            @include('messages/message')


                            {!! Form::open(array('url' => 'admin/users/0', 'id' => 'all_users_form')) !!}
                                {{-- Spoof the delete verb to access delete method see tutorial from https://scotch.io/tutorials/simple-laravel-crud-with-resource-controllers --}}
                                <input type="hidden" name="_method" value="DELETE"/>
                            <div class="row">
                            <div class="col-sm-12">
                            <table class="table table-hover table-bordered table-striped">
                                <tr>
                                    <th><input type="checkbox" id="check_all_table_items" /></th>
                                    <th>Username</th>
                                    <th>Firstname</th>
                                    <th>Lastname</th>
                                    <th>Email</th>
                                    <th>User Type</th>
                                    <th>Created</th>
                                    <th>Actions</th>
                                </tr>

                                @if( !empty($users) )
                                @foreach($users as $u)
                                <tr>
                                    <td><input type="checkbox" name="id[]" value="{{ $u->id }}" id="user_{{ $u->id }}" class="check-all-table-item"/></td>
                                    <td><a href="{{ url('admin/users/' . $u->id . '/edit') }}"> {{ $u->username }} </a></td>
                                    <td>{{ $u->firstname }}</td>
                                    <td>{{ $u->lastname }}</td>
                                    <td>{{ $u->email }}</td>
                                    <td>{{ $u->usertype_string }}</td>
                                    <td>{{ $u->created_at->format('M d, Y') }}</td>
                                    <td>
                                        <div class="table-tools">
                                            <a href="{{ url('admin/users/' . $u->id . '/edit') }}" title="Edit User"><i class="fa fa-edit"></i></a>
                                            <a href="#" title="Delete User" class="del-table-itm" data-target="#user_{{ $u->id }}" data-target-form="#all_users_form"><i class="fa fa-trash-o"></i></a>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                                @endif

                            </table>
                            </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-2">
                                    <input class="btn btn-block btn-default" type="submit" name="delete" value="Delete Selected"/>
                                </div>
                                <div class="col-sm-10">
                                    <div class="pagination-block">
                                    {!! $pagination !!}
                                    </div>
                                </div>
                            </div>

                            {!! Form::close() !!}

                        </div>
                    </div>
                       {{-- Users Box End--}}

                </div>
                </div>

            </section>


        </div>
        {{-- End Content Wrapper --}}
@endsection