@extends('admin/main')

@section('content')
    <div class="content-wrapper">

    <section class="content-header">
        <h1>Users <small>Create</small></h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('admin/dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="{{ url('admin/users') }}"><i class="fa fa-users"></i> Users</a></li>
            <li class="active">Create</li>
        </ol>
    </section>

    <section class="content">

        {{-- Show Success message if any --}}
        @include('messages/message')

        {!! Form::open( array('url' => 'admin/users', 'autocomplete' => 'off') ) !!}

        <div class="row">
        <div class="col-xs-12 col-md-12 col-lg-8">

            <div class="box box-primary">

                <div class="box-body">

                        <div class="form-group">
                            <label for="username">Username</label>
                            <input class="form-control" type="text" name="username" value="{{ $username or Request::old('username') }}" id="username" autocomplete="off"/>
                        </div>

                        <div class="form-group">
                            <label for="email">Email</label>
                            <input class="form-control" type="text" name="email" value="{{ $email or Request::old('email') }}" id="email"/>
                        </div>


                        <div class="form-group">
                            <label for="password">Password</label>
                            <input class="form-control" type="password" name="password"  id="password"/>
                        </div>

                        <div class="form-group">
                            <label for="firstname">Firstname</label>
                            <input class="form-control" type="text" name="firstname" value="{{ $firstname or Request::old('firstname') }}" id="firstname"/>
                        </div>

                        <div class="form-group">
                            <label for="lastname">Lastname</label>
                            <input class="form-control" type="text" name="lastname" value="{{ $lastname or Request::old('lastname') }}" id="lastname"/>
                        </div>

                </div>

            </div>

        </div>
        </div>

        <a href="{{ url('admin/users') }}" class="btn btn-default">Back</a>
        <input type="submit" name="save_user" value="Save" class="btn btn-primary"/>

        {!! Form::close() !!}

    </section>

    </div>
@endsection