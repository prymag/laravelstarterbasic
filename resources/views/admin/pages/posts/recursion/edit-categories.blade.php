@if( !empty($categories) )

    <?php
        $space = '';
        if( $ctr > 0  ){
            for($i = 0; $i < $ctr; $i++)
                $space .= '&nbsp;-&nbsp;';
        }
    ?>


    <option value="{{ $categories->id }}" @if( $post->category_id == $categories->id )selected="selected"@endif>{{ $space . $categories->name }}</option>

    <?php $child = $categories->children()->get(); ?>
    @if( $child->count() > 0 )
        @foreach($child as $child_category)
            @include('admin/pages/posts/recursion/edit-categories', ['categories' => $child_category, 'ctr' => $ctr+=1])
        @endforeach
    @endif



@endif