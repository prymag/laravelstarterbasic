@extends('admin/main')

@section('content')
    <div class="content-wrapper">

    <section class="content-header">
        <h1>Pages <small>Create</small></h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('admin/dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="{{ url('admin/pages') }}"><i class="fa fa-file"></i> Pages</a></li>
            <li class="active">Create</li>
        </ol>
    </section>

    <section class="content">

        @include('messages/message')

        {!! Form::open( array('url' => 'admin/pages/', 'id' => 'page_edit_form' ) ) !!}

        <div class="row">

            <div class="col-xs-8">
                <div class="box">
                    <div class="box-header">
                        <h4>Page Details</h4>
                    </div>
                    <div class="box-body">

                        <div class="form-group">
                            <label for="page_title">Page Title</label>
                            <input class="form-control" type="text" id="page_title" name="page_title" value="{{ $page_name or Request::old('page_title') }}"/>
                        </div>

                        <div class="form-group">
                            <label for="parent">Parent Page</label>
                            <select name="parent" id="parent" class="form-control">
                                <option value="0"></option>
                                @if( !empty($pages) )
                                    @foreach($pages as $p)
                                        @include('admin/pages/pages/recursion/create-parent-pages', ['pages' => $p, 'ctr' => 0])
                                    @endforeach
                                @endif
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="editor">Content</label>
                            <textarea name="content" id="content" cols="30" rows="10" class="form-control">{{ $content or Request::old('content') }}</textarea>
                        </div>


                    </div>
                </div>
            </div>

            <div class="col-xs-4">
                <div class="box">
                    <div class="box-header">
                        <h4>Meta Data</h4>
                    </div>
                    <div class="box-body">

                        <div class="form-group">
                            <label for="meta_title">Meta Title</label>
                            <input class="form-control" type="text" name="meta_title" value="{{ $page_title or Request::old('meta_title')}}" id="meta_title"/>
                        </div>

                        <div class="form-group">
                            <label for="meta_desc">Meta Description</label>
                            <textarea name="meta_desc" id="meta_desc" cols="10" rows="3" class="form-control">{{ $meta_desc or Request::old('meta_desc') }}</textarea>
                        </div>

                        <div class="form-group">
                            <label for="meta_keywords">Meta Keywords</label>
                            <textarea name="meta_keywords" id="meta_keywords" cols="10" rows="3" class="form-control">{{ $meta_keywords or Request::old('meta_keywords') }}</textarea>
                        </div>

                    </div>
                </div>
            </div>

        </div>

        <div class="row">

            <div class="col-xs-12">
            <a href="{{ url('admin/pages') }}" class="btn btn-default">Back</a>
            <input type="submit" name="save_page" value="Save" class="btn btn-primary"/>
            </div>

        </div>

        {!! Form::close() !!}

    </section>



    </div>
@endsection

@section('scripts')
    <script src="https://cdn.ckeditor.com/4.4.3/standard/ckeditor.js"></script>
    <script type="text/javascript">
        $(function () {
            // Replace the <textarea id="editor1"> with a CKEditor
            // instance, using default configuration.
            CKEDITOR.replace('content');
          });
    </script>
@endsection