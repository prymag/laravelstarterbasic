@extends('admin/main')
@section('content')
    <div class="content-wrapper">

    <section class="content-header">
        <h1>Pages</h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('admin') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active"><i class="fa fa-file"></i> Pages</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
        <div class="col-xs-12">

            {{-- Pages Box Start --}}
            <div class="box box-primary">

                <div class="box-header">
                    <div class="row">

                    <div class="col-sm-2">
                        <a class="btn btn-block btn-primary" href="{{ url('admin/pages/create') }}">Add Page</a>
                    </div>

                    </div>
                </div>

                <div class="box-header">
                    <div class="row">
                        <div class="col-sm-6">

                            <div class="show-items form-inline">
                            {!! Form::open( array('id' => 'show_item_form', 'method' => 'get') ) !!}
                            <label>
                            Show
                            <select class="select-show-item form-control" name="show">
                                <option value="25" @if( $show == '25'){{ 'selected="selected"' }}@endif>25</option>
                                <option value="50" @if( $show == '50'){{ 'selected="selected"' }}@endif>50</option>
                                <option value="all" @if( $show == '0'){{ 'selected="selected"' }}@endif>All</option>
                            </select>

                            @if( isset($page) )
                                <input type="hidden" name="page" value="{{ $page }}"/>
                            @elseif( isset($s) )
                                <input type="hidden" name="search" value="{{ $s }}"/>
                            @endif

                            pages
                            </label>
                            {!! Form::close() !!}
                            </div>

                        </div>

                        <div class="col-sm-3 col-sm-offset-3">

                            <div class="data-search">

                            {!! Form::open(array('method' => 'get', 'url' => 'admin/pages/search')) !!}
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-search"></i></span>
                                    <input class="form-control" type="text" placeholder="Search" name="search" value="{{ $s or '' }}"/>
                                    <span class="input-group-btn">
                                        <input type="submit" class="btn btn-info btn-flat" type="button" value="Go">
                                    </span>
                                </div>
                            {!! Form::close() !!}


                            </div>

                        </div>

                    </div>
                </div>

                <div class="box-body dataTables_wrapper">

                    @include('messages/message')

                    {!! Form::open(array('url' => 'admin/pages/0', 'id' => 'all_pages_form')) !!}
                        {{-- Spoof the delete verb to access delete method see tutorial from https://scotch.io/tutorials/simple-laravel-crud-with-resource-controllers --}}
                        <input type="hidden" name="_method" value="DELETE"/>
                    <div class="row">
                    <div class="col-sm-12">
                    <table class="table table-hover table-bordered table-striped">
                        <tr>
                            <th><input type="checkbox" id="check_all_table_items" /></th>
                            <th>Title</th>
                            <th>Url Slug</th>
                            <th>Parent</th>
                            <th>Created</th>
                            <th>Actions</th>
                        </tr>

                        @if( !empty($pages) )
                        @foreach($pages as $p)
                        <tr>
                            <td><input type="checkbox" name="id[]" value="{{ $p->id }}" id="page_{{ $p->id }}" class="check-all-table-item"/></td>
                            <td><a href="{{ url($p->UrlPath) }}" target="_blank"> {{ $p->title }} </a></td>
                            <td>{{ $p->url_slug }}</td>
                            <td>{{ $p->parent_name }}</td>
                            <td>{{ $p->created_at->format('M d, Y') }}</td>
                            <td>
                                <div class="table-tools">
                                    <a href="{{ url('admin/pages/' . $p->id . '/edit') }}" title="Edit Page"><i class="fa fa-edit"></i></a>
                                    <a href="#" title="Delete Page" class="del-table-itm" data-target="#page_{{ $p->id }}" data-target-form="#all_pages_form"><i class="fa fa-trash-o"></i></a>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                        @endif

                    </table>
                    </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-2">
                            <input class="btn btn-block btn-default" type="submit" name="delete" value="Delete Selected"/>
                        </div>
                        <div class="col-sm-10">
                            <div class="pagination-block">
                            {!! $pagination !!}
                            </div>
                        </div>
                    </div>

                    {!! Form::close() !!}

                </div>
            </div>
               {{-- Pages Box End--}}

        </div>
        </div>

    </section>


    </div>
    {{-- End Content Wrapper --}}
@endsection