@if( !empty($pages) )

    <?php
        $space = '';
        if( $ctr > 0  ){
            for($i = 0; $i < $ctr; $i++)
                $space .= '&nbsp;-&nbsp;';
        }
    ?>

    @if( $pages->id != $page->id)
    <option value="{{ $pages->id }}" @if( $pages->id == $page->parent_id )selected="selected"@endif>{{ $space . $pages->title }}</option>

    <?php $child = $pages->children()->get(); ?>
    @if( $child->count() > 0 )
        @foreach($child as $child_page)
            @include('admin/pages/pages/recursion/edit-parent-pages', ['pages' => $child_page, 'ctr' => $ctr+=1])
        @endforeach
    @endif

    @endif

@endif