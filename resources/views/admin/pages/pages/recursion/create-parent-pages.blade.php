@if( !empty($pages) )
{{debug($ctr)}}
    <?php
        $space = '';
        if( $ctr > 0  ){
            for($i = 0; $i < $ctr; $i++)
                $space .= '&nbsp;-&nbsp;';
        }
    ?>


    <option value="{{ $pages->id }}">{{ $space . $pages->title }}</option>

    <?php $child = $pages->children()->get(); ?>
    @if( $child->count() > 0 )
        @foreach($child as $child_page)
            @include('admin/pages/pages/recursion/create-parent-pages', ['pages' => $child_page, 'ctr' => $ctr+=1])
        @endforeach
    @endif



@endif