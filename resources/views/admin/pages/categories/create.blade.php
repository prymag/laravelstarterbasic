@extends('admin/main')

@section('content')
    <div class="content-wrapper">

    <section class="content-header">
        <h1>Category <small>Create</small></h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('admin/dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="{{ url('admin/post-categories') }}"><i class="fa fa-thumb-tack"></i> Categories</a></li>
            <li class="active">Create</li>
        </ol>
    </section>

    <section class="content">

        @include('messages/message')

        {!! Form::open( array('url' => 'admin/post-categories/', 'id' => 'category_edit_form' ) ) !!}

        <div class="row">

            <div class="col-xs-8">
                <div class="box">
                    <div class="box-header">
                        <h4>Category Details</h4>
                    </div>
                    <div class="box-body">

                        <div class="form-group">
                            <label for="name">Category Name</label>
                            <input class="form-control" type="text" name="name" id="name" value="{{ $name or Request::old('name') }}"/>
                        </div>

                        <div class="form-group">
                            <label for="parent">Category Parent</label>
                            <select name="parent" id="parent" class="form-control">
                                <option></option>
                                @if( !empty($categories) )
                                    @foreach($categories as $c)
                                        @include('admin/pages/categories/recursion/create-parent-categories', ['categories' => $c, 'ctr' => 0])
                                    @endforeach
                                @endif
                            </select>
                        </div>



                    </div>
                </div>
            </div>

            <div class="col-xs-4">
                <div class="box">
                    <div class="box-header">
                        <h4>Meta Data</h4>
                    </div>
                    <div class="box-body">

                        <div class="form-group">
                            <label for="meta_title">Meta Title</label>
                            <input class="form-control" type="text" name="meta_title" value="{{ $page_title or Request::old('meta_title')}}" id="meta_title"/>
                        </div>

                        <div class="form-group">
                            <label for="meta_desc">Meta Description</label>
                            <textarea name="meta_desc" id="meta_desc" cols="10" rows="3" class="form-control">{{ $meta_desc or Request::old('meta_desc') }}</textarea>
                        </div>

                        <div class="form-group">
                            <label for="meta_keywords">Meta Keywords</label>
                            <textarea name="meta_keywords" id="meta_keywords" cols="10" rows="3" class="form-control">{{ $meta_keywords or Request::old('meta_keywords') }}</textarea>
                        </div>

                    </div>
                </div>
            </div>

        </div>

        <div class="row">

            <div class="col-xs-12">
            <a href="{{ url('admin/post-categories') }}" class="btn btn-default">Back</a>
            <input type="submit" name="save_page" value="Save" class="btn btn-primary"/>
            </div>

        </div>

        {!! Form::close() !!}

    </section>



    </div>
@endsection
