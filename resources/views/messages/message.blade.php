@if( Session::get('success') )
<div class="alert alert-success">
    <button class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4><i class="icon fa facheck"> Success! </i></h4>
    <p>{{ Session::get('success') }}</p>
</div>
@elseif( Session::get('error') )
<div class="alert alert-danger">
    <button class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4><i class="icon fa fa-ban"></i> Error!</h4>
    <p>{{ Session::get('error') }}</p>
</div>
@elseif( $errors->any() )
<div class="alert alert-danger alert-dismissable">
    <button class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4><i class="icon fa fa-ban"></i> Error!</h4>
     <ul>
    @foreach($errors->all() as $e)
        <li>{{ $e }}</li>
    @endforeach
    </ul>
</div>
@endif