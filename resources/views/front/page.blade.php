@extends('front/index')

@section('content')

        <section class="content">
            <div class="row">
            <div class="col-lg-12">

            {!! $page->content !!}

            @if( isset($posts) )
                @include('front/includes/posts')
            @endif


            </div>
            </div>
        </section>

@endsection