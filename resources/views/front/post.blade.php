@extends('front/index')

@section('content')

        <section class="content">
            <div class="row">
            <div class="col-lg-12">

                <h1>{{ $page->title }}</h1>
                <div class="post-info">
                    <span class="date"> <span class="fa fa-clock-o"></span> {{ $page->created_at->format('M d, Y') }}</span>
                    <span class="category"> <span class="fa fa-folder"></span> {{ $page->CategoryName }}</span>
                </div>


            {!! $page->content !!}




            </div>
            </div>
        </section>

@endsection