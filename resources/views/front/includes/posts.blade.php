<div class="row">
    <div class="col-md-8 col-md-offset-2">

        @if( $posts->count() > 0 )
        <ul class="blogs-list">
            @foreach($posts as $p)
                <li>
                    <h1 class="post-title"><a href="{{ $p->UrlPath }}">{{ $p->title }}</a></h1>
                    <div class="post-info">
                        <span class="date"> <span class="fa fa-clock-o"></span> {{ $p->created_at->format('M d, Y') }}</span>
                        <span class="category"> <span class="fa fa-folder"></span> {{ $p->CategoryName }}</span>
                    </div>
                    {!! str_limit($p->content, $limit = 100, $end = '...') !!}
                </li>
            @endforeach
        </ul>
        @endif

    </div>
</div>