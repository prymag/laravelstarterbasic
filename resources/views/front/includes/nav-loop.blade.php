@if( !empty($pages) )

    <?php $child = $pages->children()->get(); ?>
    <li @if($child->count() > 0)class="dropdown dropdown-toggle level-{{$ctr}}"@else class="level-{{$ctr}}" @endif>
        <a href="@if($pages->id != $settings->where('key', 'homepage')->first()->value){{ url($pages->UrlPath) }}@else{{ '/' }}@endif">{{ $pages->title }}
        @if($child->count() > 0)
            @if($ctr > 0)
            <b class="caret-right"></b>
            @else
            <b class="caret"></b>
            @endif
        @endif
    </a>


    @if( $child->count() > 0 )
        <ul class="dropdown-menu" role="menu">
        @foreach($child as $child)
            @include('front/includes/nav-loop', ['pages' => $child, 'ctr' => $ctr+=1])
        @endforeach
        </ul>
    @endif

    </li>

@endif