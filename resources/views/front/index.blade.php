<!DOCTYPE html>
<html>
    <head>
    <meta charset="UTF-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="{{ $page->meta_description or '' }}">
    <meta name="author" content="">

    <title>{{ $page->title or '' }}</title>

    <!-- Bootstrap Core CSS -->
    <link href="{{ asset('plugins/bootstrap/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="{{ asset('plugins/IEsupport/html5shiv.min.js') }}"></script>
        <script src="{{ asset('plugins/IEsupport/respond.min.js') }}"></script>
    <![endif]-->

    </head>
<body>



    <!-- Navigation -->

    <nav class="navbar navbar-inverse navbar-static-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/">{{ $settings->where('key', 'sitename')->first()->value }}</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    @if( !empty($pages) )
                    @foreach($pages as $p)
                        @include('front/includes/nav-loop', ['pages' => $p, 'ctr' => 0])
                    @endforeach
                    @endif
                </ul>
                @if( Auth::guest())
                <ul class="nav navbar-nav navbar-right">
                    <li class=""><a href="{{ url('auth/login') }}"><i class="fa fa-sign-in"></i> Login</a></li>
                    <li class=""><a href="{{ url('register') }}"><i class="fa fa-lock"></i> Register</a></li>
                </ul>
                @else
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown user-menu">
                        <a href="{{ url('admin/profile') }}" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> {{ Auth::user()->firstname }}</a>
                        <ul class="dropdown-menu">
                                <li class="user-header">
                                    <img src="{{ asset(Auth::user()->UserImagepath) }}" class="img-circle" alt="User Image">
                                    <p> {{ Auth::user()->firstname . ' ' . Auth::user()->lastname }}</p>
                                </li>
                                <li class="user-btns">
                                    <a href="{{ url('admin/dashboard') }}" target="_blank" class="btn btn-sm btn-default">Admin Dashboard</a>
                                    <a href="{{ url('admin/profile') }}" class="btn btn-sm btn-default">Edit Profile</a>
                                    <a href="{{ url('auth/logout') }}" class="btn btn-sm btn-default">Logout</a>
                                </li>


                            </ul>
                    </li>
                </ul>
                @endif

            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>


    <div class="container">
        @yield('content')
    </div>

    <footer>
        <div class="container">
        <div class="row">
            <div class="col-lg-12">
                Copyright &copy; <a href="/">{{ $settings->where('key', 'sitename')->first()->value }}</a>
            </div>
        </div>
        </div>
    </footer>

    <script src="{{ asset('plugins/jQuery/jQuery-1.11.1.min.js') }}"></script>
    <script src="{{ asset('plugins/bootstrap/bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/front/scripts.js') }}"></script>


</body>
</html>