@extends('...front.index')

@section('content')
    <section class="content">
        <div class="row">
        <div class="col-md-6 col-md-offset-3">

        <div class="panel panel-default">

        <div class="panel-heading">
            Register
        </div>

        <div class="panel-body">

        {!! Form::open( array('id' => 'form_register') ) !!}

            @include('messages/message')

            <div class="form-group has-feedback">
                <input type="text" class="form-control" placeholder="First Name" name="firstname" value="{{ Request::old('firstname') }}" />
                <span class="glyphicon glyphicon-user form-control-feedback"></span>
            </div>

            <div class="form-group has-feedback">
                <input type="text" class="form-control" placeholder="Last Name" name="lastname" value="{{ Request::old('lastname') }}" />
                <span class="glyphicon glyphicon-user form-control-feedback"></span>
            </div>

            <div class="form-group has-feedback">
                <input type="text" class="form-control" placeholder="User Name" name="username" value="{{ Request::old('username') }}"/>
                <span class="glyphicon glyphicon-user form-control-feedback"></span>
            </div>

            <div class="form-group has-feedback">
                <input type="email" class="form-control" placeholder="Email" name="email" value="{{ Request::old('email') }}" />
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>

            <div class="form-group has-feedback">
                <input type="password" class="form-control" placeholder="Password" name="password">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>

            <div class="form-group has-feedback">
                <input type="password" class="form-control" placeholder="Password Confirmation" name="password_confirmation">
                <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
            </div>

            <div class="row">
                <div class="col-xs-8"></div>
                <div class="col-xs-4">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Register</button>
                </div>
            </div>

        {!! Form::close() !!}

        </div> {{-- /.panel-body --}}

        </div>


        </div>
        </div>
    </section>
@endsection