<?php namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Posts;
use App\Models\Settings;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\PasswordBroker;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class PasswordController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Password Reset Controller
	|--------------------------------------------------------------------------
	|
	| This controller is responsible for handling password reset requests
	| and uses a simple trait to include this behavior. You're free to
	| explore this trait and override any methods you wish to tweak.
	|
	*/

	use ResetsPasswords;

    private $vars;

    protected $redirectPath = '/';

	/**
	 * Create a new password controller instance.
	 *
	 * @param  \Illuminate\Contracts\Auth\Guard  $auth
	 * @param  \Illuminate\Contracts\Auth\PasswordBroker  $passwords
	 * @return void
	 */
	public function __construct(Guard $auth, PasswordBroker $passwords, Request $request)
	{
		$this->auth = $auth;
		$this->passwords = $passwords;

		$this->middleware('guest');

        $this->vars['settings'] = Settings::all();
        $this->vars['pages'] = Posts::where('post_type', '=', 'page')->where('parent_id', '=', '0')->get();


	}

    public function getEmail(){

        return view('auth/password', $this->vars);

    }

    public function getReset($token = null)
    {
        if (is_null($token))
        {

            throw new NotFoundHttpException;
        }

        return view('auth.reset', $this->vars)->with('token', $token);
    }

}
