<?php namespace App\Http\Controllers\Auth;

use App\Models\Posts;
use App\Models\Settings;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;


class AuthenticateController extends Controller {

	//

    private $vars, $view, $loginPath, $redirectPath;

    protected $redirectTo = '/';

    public function __construct(Request $request){

        $this->vars['settings'] = Settings::all();
        $this->vars['pages'] = Posts::where('post_type', '=', 'page')->where('parent_id', '=', '0')->get();
        $this->view = 'auth/login';
        $this->loginPath = 'auth/login';
        $this->redirectPath = '/';

    }

    public function  getIndex(){

    }

    public function  getLogin(){

        return view($this->view, $this->vars);

    }

    public function  postLogin(Request $request){


        $rules = [
            'username' => 'required',
            'password' => 'required'
        ];

        $inputs = $request->all();

        $v = Validator::make($inputs, $rules);

        if( $v->fails() )
            return redirect($this->loginPath)->withErrors($v);
        else {

            if( Auth::attempt( array('username' => $inputs['username'], 'password' => $inputs['password'] ), $request->has('remember') ) ) {
                return redirect($this->redirectPath);
            }
            else if( Auth::attempt( array('email' => $inputs['username'], 'password' => $inputs['password'] ), $request->has('remember') ) ) {
                return redirect($this->redirectPath);
            }
            else {
                return redirect($this->loginPath)->with(['error' => 'User not found!']);
            }


        }


    }

    public function getLogout(){

        Auth::logout();
        return redirect('/');

    }

    public function missingMethod($parameters = array())
    {
        //
        return response()->view('errors/404', $this->vars, 404);
    }




}
