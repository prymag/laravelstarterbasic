<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Posts;
use App\Models\Settings;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class PageController extends Controller {

	//
    public function index($path){

        $vars = [];
        $vars['settings'] = Settings::all();
        $vars['pages'] = Posts::where('post_type', '=', 'page')->where('parent_id', '=', '0')->get();
        $posts = null;


        // Get the homepage
        if( $path == '/'){
            $homepage_id = Settings::where('key', '=', 'homepage')->get()->first()->value;
            $vars['page'] = Posts::find($homepage_id);
            if( is_null($vars['page']) )
                return 'No homepage defined';
        }
        else{
            $path_array = explode('/', $path);

            // Get page/post by url slug
            $slug = end($path_array);
            $vars['page'] = Posts::where('url_slug', $slug)->get()->first();

            // Check if page exists
            if( empty($vars['page']) ) {
                return response()->view('errors/404', $vars, 404);
            }

            // Check if this is the homepage, then redirect if it is
            if( $vars['page']->id == $vars['settings']->where('key', 'homepage')->first()->value )
                return redirect('/');

            // Check if the url path is correct
            $page_url = $vars['page']->UrlPath;
            if( $page_url != $path )
                return response()->view('errors/404', $vars, 404);

            // Check if this page is posts page
            if( $vars['settings']->where('key', 'postspage')->first()->value == $vars['page']->id )
                $vars['posts'] = Posts::where('post_type', '=', 'post')->get();

        }

        $view = $vars['page']->post_type == 'page' ? 'page' : 'post';

        return view('front/'.$view, $vars);

    }


    public function register(){
        $vars['settings'] = Settings::all();
        $vars['pages'] = Posts::where('post_type', '=', 'page')->where('parent_id', '=', '0')->get();

        $page = new \stdClass();
        $page->title = 'Register';
        $vars['page'] = $page;

        return view('auth/register', $vars);
    }

    public function postRegister(Request $request){

        $rules = User::getValidationRules('registration');

        $inputs = $request->all();

        $v = Validator::make($inputs, $rules);

        if( $v->fails() ){
            return redirect('register')->withErrors($v)->withInput($inputs);
        }
        else{
            /* Save the user */
            $user = new User;
            $user->username = $request->get('username');
            $user->password = Hash::make($request->get('password'));
            $user->email = $request->get('email');
            $user->firstname = $request->get('firstname');
            $user->lastname = $request->get('lastname');
            $user->usertype = 3;

            $user->save();
            return redirect('/register')->with(['success' => 'Registration successful']);


        }

    }





}
