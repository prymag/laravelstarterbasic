<?php namespace App\Http\Controllers\Admin;

use App\Libs\BlueimpFileupload\UploadsHandler;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class AjaxController extends Controller {

    public function UploadUserImage(Request $request){

            if( $request->ajax()  ) {
                $options = array(
                    'upload_dir' => 'images/users/',
                    'upload_url' => 'http://'.$request->getHost().'/images/users/',
                    'print_response' => false, // Set to false to retrieve raw data
                    'user_dirs' => true,
                    'user_id' => $request->get('user_id'),
                    'image_versions' => array(),
                    'image_name' => date_timestamp_get(date_create())
                );
                $uploads = new UploadsHandler($options);
                return response()->json($uploads->response['files']);
            }
            else{
                return 'No direct access';
            }

    }

    public function deleteUserImage(Request $request){

        if( $request->ajax() ){
            $user_id = $request->get('user_id');
            $image_name = $request->get('image_name');
            $user_image = public_path().'/images/users/' . $user_id . '/' . $image_name;

            if( !File::delete($user_image) ){
                return 'File not deleted';
            }
            else{
                return 'Success';
            }

        }
        else{
            return 'No direct access';
        }

    }

}