<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller {

	//

    function getIndex(){
        return redirect('admin/dashboard');

    }

    function getDashboard(){

        /* Set variables */
        $template = 'dashboard';
        $pagename = 'dashboard';
        $pagetitle = 'Dashboard';


        return view('admin/pages/dashboard', compact('pagetitle', 'template', 'pagename'));
    }




}
