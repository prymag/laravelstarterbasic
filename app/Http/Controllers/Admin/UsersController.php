<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;


class UsersController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	public function index(Request $request)
	{
		//
        $pagename = 'users';
        $pagetitle = 'Users';

        // Get users and set pagination
        $show = $request->get('show') ? $request->get('show') : 25;
        $show = $show == 'all' ? 0 : $show;

        $users = User::orderBy('id', 'desc')->paginate($show);

        $appends = array();
        if( $show )
            $appends['show'] = $show;

        $pagination = $users->appends($appends)->render();

        return view('admin/pages/users/index', compact('pagename', 'pagetitle', 'users', 'pagination', 'show'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
        $pagename = 'users';
        $pagetitle = 'Create Users';
        return view('admin/pages/users/create', compact('pagename','pagetitle'));

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		// Get required variables except submit
        $inputs = $request->except( array('save_user', '_token') );

        // Get Validation Rules
        $rules = User::getValidationRules('create');

        $v = Validator::make($inputs, $rules);

        if( $v->fails() ){
            return redirect('admin/users/create')->withErrors($v)->withInput($inputs);
        }
        else{
            /* Save the user */
            $user = new User;
            $user->username = $request->get('username');
            $user->password = Hash::make($request->get('password'));
            $user->email = $request->get('email');
            $user->firstname = $request->get('firstname');
            $user->lastname = $request->get('lastname');
            $user->usertype = 3;

            $user->save();
            return redirect('admin/users/create')->with(['success' => 'User added sucessfully.']);
            

        }

	}



	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{

		//
        $user = new User;
        $user = $user->find($id);

        // Get predefined avatars
        $files = File::allFiles(base_path().'/public/images/avatars');
        $avatars = array();
        foreach($files as $f){
            $avatars[] = pathinfo($f);
        }

        // Get user avatars
        $user_path = base_path().'/public/images/users/'.$user->id;
        $user_avatars = array();
        if( File::exists($user_path) ) {
            $files = File::allFiles($user_path);
            foreach ($files as $f) {
                $user_avatars[] = pathinfo($f);
            }
        }

        $pagename = 'users';
        $pagetitle = 'Edit User';


        return view('admin/pages/users/edit', compact('pagename', 'template', 'pagetitle', 'user', 'user_avatars', 'avatars'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		//

        // Get required variables except submit
        $inputs = $request->except( array('save_user', '_token', '_method') );

        // Set Validation Rules
        $rules = [
            'username' => 'required|unique:users,username,'.$id.'|min:5',
            'email' => 'required|unique:users,email,'.$id.'|email',
            'firstname' => 'required',
            'lastname' => 'required'
        ];

        // Add password rule if password is set;
        if( $request->get('password') ){
            $rules['password'] = 'min:8';
        }

        $v = Validator::make($inputs, $rules);

        if( $v->fails() ){
            return redirect('admin/users/'.$id.'/edit')->withErrors($v)->withInput($inputs);
        }
        else {
            /* Save the user */
            $user = User::find($id);
            $user->username = $request->get('username');
            // Update password if password field is not empty
            if( $request->get('password') )
                $user->password = Hash::make($request->get('password'));

            $user->use_custom_image = $request->get('use_custom_image');
            $user->user_image = $request->get('selected_user_image');
            $user->email = $request->get('email');
            $user->firstname = $request->get('firstname');
            $user->lastname = $request->get('lastname');

            $user->save();
            return redirect('admin/users/'.$id.'/edit')->with(['success' => 'User updated sucessfully.']);
        }
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id, Request $request)
	{
		//
        $items = $request->get('id') ? $request->id : $id;

        if( empty($items) ){
            return redirect('admin/users')->with('error', 'Please select a user item to delete');
        }
        $user = new User;
        $r = $user->destroy($items);

        if( $r ){
            return redirect('admin/users/')->with('success', 'User/s deleted successfully');
        }



	}


    /* Search for users
     *
     * */
    public function search(Request $request){

        $s = $request->get('search');

        if( empty($s) )
            return redirec('admin/users')->with('error', 'Please enter a value to search');

        $pagename = 'users';
        $template = 'users';
        $pagetitle = 'Users search: '.$s;

        // Get users and set pagination
        $show = $request->get('show') ? $request->get('show') : 25;
        $show = $show == 'all' ? 0 : $show;

        $users = User::orderBy('id', 'desc')->where('username', 'like', '%'.$s.'%')
            ->paginate($show);
        // Set appends for pagination links
        $appends = array();
        $appends['search'] = $s;

        if( $request->get('page') )
            $appends['page'] = $request->get('page');
        if( $show )
            $appends['show'] = $show;

        $pagination = $users->appends($appends)->render();

        return view('admin/pages/users/index', compact('template', 'pagename', 'pagetitle', 'users', 's', 'pagination', 'show'));

    }

    public function profile(){

        //
        $user = Auth::user();

        // Get predefined avatars
        $files = File::allFiles(base_path().'/public/images/avatars');
        $avatars = array();
        foreach($files as $f){
            $avatars[] = pathinfo($f);
        }

        // Get user avatars
        $user_path = base_path().'/public/images/users/'.$user->id;
        $user_avatars = array();
        if( File::exists($user_path) ) {
            $files = File::allFiles($user_path);
            foreach ($files as $f) {
                $user_avatars[] = pathinfo($f);
            }
        }

        $pagename = 'profile';
        $pagetitle = 'Edit Profile';


        return view('admin/pages/users/profile', compact('pagename', 'template', 'pagetitle', 'user', 'user_avatars', 'avatars'));

    }

    public function saveProfile(Request $request) {
        $inputs = $request->except( array('save_profile', '_token', '_method') );

        // Set Validation Rules
        $rules = [
            'username' => 'required|unique:users,username,'.Auth::user()->id.'|min:5',
            'email' => 'required|unique:users,email,'.Auth::user()->id.'|email',
            'firstname' => 'required',
            'lastname' => 'required'
        ];

        // Add password rule if password is set;
        if( $request->get('password') ){
            $rules['password'] = 'min:8';
        }

        $v = Validator::make($inputs, $rules);

        if( $v->fails() ){
            return redirect('admin/profile/')->withErrors($v)->withInput($inputs);
        }
        else {
            /* Save the user */
            $user = Auth::user();
            $user->username = $request->get('username');
            // Update password if password field is not empty
            if( $request->get('password') )
                $user->password = Hash::make($request->get('password'));

            $user->use_custom_image = $request->get('use_custom_image');
            $user->user_image = $request->get('selected_user_image');
            $user->email = $request->get('email');
            $user->firstname = $request->get('firstname');
            $user->lastname = $request->get('lastname');

            $user->save();
            return redirect('admin/profile')->with(['success' => 'User updated sucessfully.']);
        }
    }

}
