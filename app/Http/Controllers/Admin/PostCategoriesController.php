<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Categories;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PostCategoriesController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
        //
        $pagename = 'post-categories';
        $pagetitle = 'Post Categories';

        // Get users and set pagination
        $show = $request->get('show') ? $request->get('show') : 25;
        $show = $show == 'all' ? 0 : $show;

        $categories = Categories::orderBy('id', 'desc')->paginate($show);

        $appends = array();
        if( $show )
            $appends['show'] = $show;

        $pagination = $categories->appends($appends)->render();

        return view('admin/pages/categories/index', compact('pagename', 'pagetitle', 'categories', 'pagination', 'show'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
        $pagename = 'post-categories';
        $pagetitle = 'Create Category';

        // Get all top level categories
        $categories = Categories::where('parent_id', '=', '0')->get();

        return view('admin/pages/categories/create', compact('pagename','pagetitle', 'categories'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
        //
        $inputs = $request->except( array('save_page', '_token') );

        $rules = [
            'name' => 'required'
        ];

        $v = Validator::make($inputs, $rules);

        if( $v->fails() ){
            return redirect('admin/post-categories/create')->withErrors($v)->withInput($inputs);
        }
        else{
            /* Save category */
            $category = new Categories();
            $category->name = $inputs['name'];
            $category->url_slug = Categories::generateSlug($inputs['name']);
            $category->parent_id = $inputs['parent'];
            $category->meta_title = $inputs['meta_title'];
            $category->meta_desc = $inputs['meta_desc'];
            $category->meta_keywords = $inputs['meta_keywords'];
            $category->save();

            return redirect('admin/post-categories/create')->with(['success' => 'Post added sucessfully.']);
        }
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
        return redirect('admin/post-categories/'.$id.'/edit');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
        $category = Categories::find($id);

        if( $category == null)
            return redirect('admin/post-categories')->with(['error' => 'Category not found']);

        $pagename = 'post-categories';
        $pagetitle = 'Edit Category';

        // Get all top level categories
        $categories = Categories::where('parent_id', '=', '0')->where('id', '!=', $id, 'AND')->get();

        return view('admin/pages/categories/edit', compact('pagename', 'pagetitle', 'category', 'categories'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		//
        $inputs = $request->except( array('save_page', '_token') );

        $rules = [
            'name' => 'required'
        ];

        $v = Validator::make($inputs, $rules);

        if( $v->fails() ){
            return redirect('admin/post-categories/' . $id . '/edit')->withErrors($v)->withInput($inputs);
        }
        else {
            $category = Categories::find($id);

            if( $category->name != $inputs['name']){
                $category->url_slug = Categories::generateSlug( $inputs['name'] );
            }

            $category->name = $inputs['name'];
            $category->parent_id = $inputs['parent'];
            $category->meta_title = $inputs['meta_title'];
            $category->meta_desc = $inputs['meta_desc'];
            $category->meta_keywords = $inputs['meta_keywords'];
            $category->save();
        }

        return redirect('admin/post-categories/'.$id.'/edit')->with(['success' => 'Category updated sucessfully.']);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id, Request $request)
	{
		//
        $items = $request->get('id') ? $request->get('id') : $id;

        if( empty($items) ){
            return redirect('admin/post-categories')->with('error', 'Please select a page item to delete');
        }

        $r = Categories::destroy($items);

        if( $r ){
            if( is_array($items) )
                Categories::whereIn('parent_id',$items)->update( array('parent_id' => '0') );
            else
                Categories::where('parent_id', '=', $items)->update( array('parent_id' => '0') );

            return redirect('admin/post-categories/')->with('success', 'Categories/s deleted successfully');
        }
	}

    /* Search for categories
     *
     * */
    public function search(Request $request){

        $s = $request->get('search');

        if( empty($s) )
            return redirect('admin/post-categories')->with('error', 'Please enter a value to search');

        $pagename = 'post-categories';
        $pagetitle = 'Category search: '.$s;

        // Get pages and set pagination
        $show = $request->get('show') ? $request->get('show') : 25;
        $show = $show == 'all' ? 0 : $show;

        $pages = Categories::orderBy('id', 'desc')->where('title', 'like', '%'.$s.'%')->paginate($show);
        // Set appends for pagination links
        $appends = array();
        $appends['search'] = $s;

        if( $request->get('page') )
            $appends['page'] = $request->get('page');
        if( $show )
            $appends['show'] = $show;

        $pagination = $pages->appends($appends)->render();

        return view('admin/pages/categories/index', compact('template', 'pagename', 'pagetitle', 'pages', 's', 'pagination', 'show'));

    }

}
