<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Categories;
use App\Models\Posts;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PostsController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
        //
        $pagename = 'posts';
        $pagetitle = 'Posts';

        // Get users and set pagination
        $show = $request->get('show') ? $request->get('show') : 25;
        $show = $show == 'all' ? 0 : $show;

        $posts = Posts::where('post_type', '=', 'post')->orderBy('id', 'desc')->paginate($show);



        $appends = array();
        if( $show )
            $appends['show'] = $show;

        $pagination = $posts->appends($appends)->render();

        return view('admin/pages/posts/index', compact('pagename', 'pagetitle', 'posts', 'pagination', 'show'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        $pagename = 'posts';
        $pagetitle = 'Create Post';
        $categories = Categories::where('parent_id', '=', '0')->get();

        return view('admin/pages/posts/create', compact('pagename','pagetitle', 'categories'));

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
        //
        $inputs = $request->except( array('save_page', '_token') );

        $rules = [
            'post_title' => 'required'
        ];

        $v = Validator::make($inputs, $rules);

        if( $v->fails() ){
            return redirect('admin/posts/create')->withErrors($v)->withInput($inputs);
        }
        else{
            /* Save page */
            $post = new Posts();
            $post->title = $inputs['post_title'];
            $post->url_slug = Posts::generateSlug( $inputs['post_title']);
            $post->content = $inputs['content'];
            $post->category_id = $inputs['category'];
            $post->meta_title = $inputs['meta_title'];
            $post->meta_desc = $inputs['meta_desc'];
            $post->meta_keywords = $inputs['meta_keywords'];
            $post->post_type = 'post';
            $post->save();

            return redirect('admin/posts/create')->with(['success' => 'Post added sucessfully.']);
        }
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        //
        $post = Posts::find($id);
        $categories = Categories::where('parent_id', '=', '0')->get();

        $pagename = 'posts';
        $pagetitle = 'Edit Post';

        return view('admin/pages/posts/edit', compact('pagename', 'pagetitle', 'post', 'categories'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		//
        //
        $inputs = $request->except( array('save_page', '_token') );

        $rules = [
            'post_title' => 'required'
        ];

        $v = Validator::make($inputs, $rules);

        if( $v->fails() ){
            return redirect('admin/posts/' . $id . '/edit')->withErrors($v)->withInput($inputs);
        }
        else {
            $post = Posts::find($id);

            if( $post->title != $inputs['post_title']){
                $post->url_slug = Posts::generateSlug( $inputs['post_title'] );
            }

            $post->title = $inputs['post_title'];
            $post->content = $inputs['content'];
            $post->category_id = $inputs['category'];
            $post->meta_title = $inputs['meta_title'];
            $post->meta_desc = $inputs['meta_desc'];
            $post->meta_keywords = $inputs['meta_keywords'];
            $post->save();
        }

        return redirect('admin/posts/'.$id.'/edit')->with(['success' => 'Post updated sucessfully.']);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id, Request $request)
	{
		//
        //
        $items = $request->get('id') ? $request->get('id') : $id;

        if( empty($items) ){
            return redirect('admin/posts')->with('error', 'Please select a page item to delete');
        }

        $r = Posts::destroy($items);

        if( $r ){

            return redirect('admin/posts/')->with('success', 'Page/s deleted successfully');
        }
	}

    /* Search for pages
     *
     * */
    public function search(Request $request){

        $s = $request->get('search');

        if( empty($s) )
            return redirect('admin/posts')->with('error', 'Please enter a value to search');

        $pagename = 'posts';
        $pagetitle = 'Posts search: '.$s;

        // Get posts and set pagination
        $show = $request->get('show') ? $request->get('show') : 25;
        $show = $show == 'all' ? 0 : $show;

        $pages = Posts::orderBy('id', 'desc')->where('title', 'like', '%'.$s.'%')->where('post_type', '=', 'post', 'AND')->paginate($show);
        // Set appends for pagination links
        $appends = array();
        $appends['search'] = $s;

        if( $request->get('page') )
            $appends['page'] = $request->get('page');
        if( $show )
            $appends['show'] = $show;

        $pagination = $pages->appends($appends)->render();

        return view('admin/pages/posts/index', compact('template', 'pagename', 'pagetitle', 'pages', 's', 'pagination', 'show'));

    }

}
