<?php namespace App\Http\Controllers\Admin;

use App\Models\Posts;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class PagesController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
        //
        $pagename = 'pages';
        $pagetitle = 'Pages';

        // Get users and set pagination
        $show = $request->get('show') ? $request->get('show') : 25;
        $show = $show == 'all' ? 0 : $show;

        $pages = Posts::where('post_type', '=', 'page')->orderBy('id', 'desc')->paginate($show);

        $appends = array();
        if( $show )
            $appends['show'] = $show;

        $pagination = $pages->appends($appends)->render();

        return view('admin/pages/pages/index', compact('pagename', 'pagetitle', 'pages', 'pagination', 'show'));

	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
        $pagename = 'pages';
        $pagetitle = 'Create Page';

        // Get top level pages,
        $pages = Posts::where('parent_id', '=', 0)->where('post_type', '=', 'page', 'AND')->get();

        return view('admin/pages/pages/create', compact('pagename','pagetitle', 'pages'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
        //
        $inputs = $request->except( array('save_page', '_token') );

        $rules = [
            'page_title' => 'required'
        ];

        $v = Validator::make($inputs, $rules);

        if( $v->fails() ){
            return redirect('admin/pages/create')->withErrors($v)->withInput($inputs);
        }
        else{
            /* Save page */
            $page = new Posts();
            $page->title = $inputs['page_title'];
            $page->url_slug = Posts::generateSlug( $inputs['page_title'], 'page' );
            $page->content = $inputs['content'];
            $page->parent_id = $inputs['parent'];
            $page->meta_title = $inputs['meta_title'];
            $page->meta_desc = $inputs['meta_desc'];
            $page->meta_keywords = $inputs['meta_keywords'];
            $page->post_type = 'page';
            $page->save();

            return redirect('admin/pages/create')->with(['success' => 'Page added sucessfully.']);
        }

	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
        $page = Posts::find($id);

        // Get all pages except current page and pages that are not a child of this page
        $pages = Posts::where('id', '!=', $id)->where('parent_id', '=', 0, 'AND')->where('post_type', '=', 'page', 'AND')->get();

        $pagename = 'pages';
        $pagetitle = 'Edit Pages';

        return view('admin/pages/pages/edit', compact('pagename', 'pagetitle', 'page', 'pages'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		//
        $inputs = $request->except( array('save_page', '_token') );

        $rules = [
            'page_title' => 'required'
        ];

        $v = Validator::make($inputs, $rules);

        if( $v->fails() ){
            return redirect('admin/pages/' . $id . '/edit')->withErrors($v)->withInput($inputs);
        }
        else {
            $page = Posts::find($id);

            if( $page->title != $inputs['page_title']){
                $page->url_slug = Posts::generateSlug( $inputs['page_title'], 'page' );
            }

            $page->title = $inputs['page_title'];
            $page->content = $inputs['content'];
            $page->parent_id = $inputs['parent'];
            $page->meta_title = $inputs['meta_title'];
            $page->meta_desc = $inputs['meta_desc'];
            $page->meta_keywords = $inputs['meta_keywords'];
            $page->save();
        }

        return redirect('admin/pages/'.$id.'/edit')->with(['success' => 'Page updated sucessfully.']);

    }

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id, Request $request)
	{
		//
        $items = $request->get('id') ? $request->get('id') : $id;

        if( empty($items) ){
            return redirect('admin/pages')->with('error', 'Please select a page item to delete');
        }

        $r = Posts::destroy($items);

        if( $r ){
            if( is_array($items) )
                Posts::whereIn('parent_id',$items)->update( array('parent_id' => '0') );
            else
                Posts::where('parent_id', '=', $items)->update( array('parent_id' => '0') );

            return redirect('admin/pages/')->with('success', 'Page/s deleted successfully');
        }

	}

    /* Search for pages
     *
     * */
    public function search(Request $request){

        $s = $request->get('search');

        if( empty($s) )
            return redirect('admin/pages')->with('error', 'Please enter a value to search');

        $pagename = 'pages';
        $pagetitle = 'Pages search: '.$s;

        // Get pages and set pagination
        $show = $request->get('show') ? $request->get('show') : 25;
        $show = $show == 'all' ? 0 : $show;

        $pages = Posts::orderBy('id', 'desc')->where('title', 'like', '%'.$s.'%')->where('post_type', '=', 'page', 'AND')->paginate($show);
        // Set appends for pagination links
        $appends = array();
        $appends['search'] = $s;

        if( $request->get('page') )
            $appends['page'] = $request->get('page');
        if( $show )
            $appends['show'] = $show;

        $pagination = $pages->appends($appends)->render();

        return view('admin/pages/pages/index', compact('template', 'pagename', 'pagetitle', 'pages', 's', 'pagination', 'show'));

    }

}
