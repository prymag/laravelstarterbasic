<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Posts;
use App\Models\Settings;
use Illuminate\Http\Request;

class SettingsController extends Controller {

	//
    public function getIndex(){
        $pagename = 'settings';
        $pagetitle = 'Site Settings';

        // Get top level pages
        $pages = Posts::where('post_type', '=', 'page')->where('parent_id', '=', '0', 'AND')->get();

        $settings = Settings::all();

        return view('admin/pages/settings/index', compact('pagename', 'pagetitle', 'pages', 'settings'));
    }

    public function postIndex(Request $request){

        $inputs = $request->all();

        Settings::where('key', 'homepage')->update(['value' => $inputs['homepage']]);
        Settings::where('key', 'postspage')->update(['value' => $inputs['postspage']]);
        Settings::where('key', 'sitename')->update(['value' => $inputs['sitename']]);

        return redirect('admin/settings/')->with(['success' => 'Site settings updated successfully!']);



    }

}
