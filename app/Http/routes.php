<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Careful when adding forward slash on routes


Route::group( ['prefix' => 'admin', 'middleware' => 'auth'], function() {

    //AJAX Routes
    Route::post('ajax/upload-user-image', 'Admin\AjaxController@UploadUserImage');
    Route::get('ajax/upload-user-image', 'Admin\AjaxController@UploadUserImage');
    Route::post('ajax/delete-user-image', 'Admin\AjaxController@DeleteUserImage');

    // User Routes
    Route::get('profile', 'Admin\UsersController@profile');
    Route::post('profile', 'Admin\UsersController@saveProfile');
    Route::get('users/search', 'Admin\UsersController@search');
    Route::resource('users', 'Admin\UsersController');

    // Pages Routes
    Route::get('pages/search', 'Admin\PagesController@search');
    Route::resource('pages', 'Admin\PagesController');

    // Post Routes
    Route::get('posts/search', 'Admin\PostsController@search');
    Route::resource('posts', 'Admin\PostsController');

    // Post Categories
    Route::get('post-categories/search', 'Admin\PostCategoriesController@search');
    Route::resource('post-categories', 'Admin\PostCategoriesController');

    // Controller routes should be placed here
    Route::controllers([
        'settings' => 'Admin\SettingsController',
        '/' => 'Admin\AdminController'
    ]);



});

Route::controllers([
    'auth' => 'Auth\AuthenticateController',
    'password' => 'Auth\PasswordController',
]);

// Front page
Route::get('register', 'PageController@register');
Route::post('register', 'PageController@postRegister');

Route::any('{url?}', ['uses' => 'PageController@index'])->where(['url' => '[-a-z0-9/]+']);



