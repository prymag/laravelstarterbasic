<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'email', 'password', 'usertype_string'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];

    private static $rules = [
        'registration' => [
            'username' => 'required|alpha_num|unique:users|min:5',
            'email' => 'required|unique:users|email',
            'password' => 'required|alpha_num|min:8|confirmed',
            'password_confirmation' => 'required|alpha_num|min:8',
            'firstname' => 'required',
            'lastname' => 'required'
        ],
        'create' => [
            'username' => 'required|alpha_num|unique:users|min:5',
            'email' => 'required|unique:users|email',
            'password' => 'required|alpha_num|min:8',
            'firstname' => 'required',
            'lastname' => 'required'
        ],
        'update' => [
            'firstname' => 'required',
            'lastname' => 'required',
            'username' => 'required|alpha_num|unique:users|min:5',
            'email' => 'required|unique:users|email'
        ],

    ];

    public function getUsertypeStringAttribute(){

        $usertype = $this->attributes['usertype'];
        switch($usertype){
            case 1:
                return 'Super Administrator';
                break;
            case 2:
                return 'Administrator';
                break;
            case 3:
                return 'User';
                break;
            default :
                'Undefined';
                break;
        }

    }

    public function getUserImagePathAttribute(){
        $user_image = $this->attributes['use_custom_image'] ? 'images/users/'.$this->attributes['id'].'/'.$this->attributes['user_image'] : 'images/avatars/'.$this->attributes['user_image'];
        return $user_image;
    }

    public static function getValidationRules($type){
        return self::$rules[$type];
    }


}
