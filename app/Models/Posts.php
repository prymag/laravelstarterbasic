<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Posts extends Model {

	//
    protected $appends = array('parent_page');

    public static function generateSlug( $pagename, $page_type = 'post' ) {

        $slug = Str::slug( $pagename );
        $slugs = Self::whereRaw("url_slug REGEXP '^{$slug}(-[0-9]*)?$'")->where('post_type', '=', $page_type, 'AND');

        if ($slugs->count() === 0) {
            return $slug;
        }

        // Get the last matching slug
        $lastSlug = $slugs->orderBy('url_slug', 'desc')->first()->slug;

        // Strip the number off of the last slug, if any
        $lastSlugNumber = intval(str_replace($slug . '-', '', $lastSlug));

        // Increment/append the counter and return the slug we generated
        return $slug . '-' . ($lastSlugNumber + 1);
    }

    public function getParentNameAttribute(){
        $parent = $this->parent()->get()->first();
        return !empty($parent) ? $parent->title : '';
    }

    public function getCategoryNameAttribute(){
        $parent = $this->parentCategory()->get()->first();
        return !empty($parent) ? $parent->name : '';
    }

    public function children(){
        return $this->hasMany('App\Models\Posts', 'parent_id', 'id');
    }

    public function parent(){
        return $this->belongsTo('App\Models\Posts', 'parent_id', 'id');
    }

    public function parentCategory(){
        return $this->belongsTo('App\Models\Categories', 'category_id', 'id');
    }

    public function getUrlPathAttribute(){
        $page = Self::where('url_slug', $this->attributes['url_slug'])->get()->first();
        return $this->_buildPageURL($page);

    }

    private function _traverseBuildParentHeirarchy($model){
        if( !empty($model) ) {
            $item['model'] = $model;
            $parent = $model->parent()->get()->first();

            if ( !empty($parent) && $parent->count() > 0) {
                $item['parent'] = $this->_traverseBuildParentHeirarchy($parent);
            }

            $parents[] = $item;
            return $parents;
        }

    }

    private function _traverseHeirarchyToURL($array ){

        foreach($array as $a){
            $url = $a['model']->url_slug . '/';
            if( isset($a['parent']) )
                $url .= $this->_traverseHeirarchyToURL($a['parent']);
        }
        return $url;

    }

    private function _buildPageURL($page){
        $parent_array = $this->_traverseBuildParentHeirarchy($page);
        $url = $this->_traverseHeirarchyToURL($parent_array);
        $url = explode('/', $url);
        $url = array_reverse($url);

        $url_string = '';
        foreach($url as $u){
            if( !empty($u) )
                $url_string .=  $u . '/';
        }

        return substr($url_string, 0, -1);
    }



}
