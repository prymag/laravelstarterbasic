<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Categories extends Model {

	//
    public static function generateSlug( $name ) {

        $slug = Str::slug( $name );
        $slugs = Self::whereRaw("url_slug REGEXP '^{$slug}(-[0-9]*)?$'");

        if ($slugs->count() === 0) {
            return $slug;
        }

        // Get the last matching slug
        $lastSlug = $slugs->orderBy('url_slug', 'desc')->first()->slug;

        // Strip the number off of the last slug, if any
        $lastSlugNumber = intval(str_replace($slug . '-', '', $lastSlug));

        // Increment/append the counter and return the slug we generated
        return $slug . '-' . ($lastSlugNumber + 1);
    }

    public function children(){
        return $this->hasMany('App\Models\Categories', 'parent_id', 'id');
    }

    public function getParentCategoryAttribute(){
        $parent_id = $this->attributes['parent_id'];

        if( $parent_id > 0){
            $c = Self::find($parent_id);
            return $c->name;
        }
        else{
            return '';
        }


    }

}
